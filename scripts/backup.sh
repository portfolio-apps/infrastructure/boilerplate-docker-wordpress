#!/bin/bash

# Set variables
DB_NAME="<DB_NAME>"
DB_USER="<DB_USER>"
DB_PASSWORD="<DB_PASSWORD>"
BACKUP_NAME="backup_$(date +%Y%m%d%H%M%S).sql"
S3_BUCKET="<BUCKET_NAME>"
S3_PATH="s3://${S3_BUCKET}/<FOLDER_IN_BUCKET>/${BACKUP_NAME}"

# MySQL backup
mysqldump -u ${DB_USER} -p${DB_PASSWORD} ${DB_NAME} > ${BACKUP_NAME}

# Check if mysqldump was successful
if [ $? -eq 0 ]; then
  echo "MySQL backup was successful."
else
  echo "Error in taking MySQL backup."
  exit 1
fi

# Upload to S3
aws s3 cp ${BACKUP_NAME} ${S3_PATH}

# Check if upload was successful
if [ $? -eq 0 ]; then
  echo "Backup successfully uploaded to S3."
else
  echo "Error in uploading backup to S3."
  exit 1
fi

# Cleanup local backup file
rm -f ${BACKUP_NAME}
echo "Local backup file removed."


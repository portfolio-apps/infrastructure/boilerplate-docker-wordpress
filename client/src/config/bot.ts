export const botScript = `Bot({
        id: "65ed45874745d458cbf57254",
        apiHost: "https://dev-api.promptengineers.ai",
        theme: {
            button: {
                backgroundColor: "#6366f1",
                hoverColor: "#4ade80",
                height: "64px",
                width: "60px",
                icon: {
                    width: "60px",
                    height: "60px",
                    borderRadius: "8px",
                },
            },
            chatWindow: {
                bottom: "95px",
                welcomeButtons: [
                    {
                        label: "Website",
                        href: "https://promptengineers.ai",
                    },
                    {
                        label: "Slack Channel",
                        href: "https://promptengineersai.slack.com/join/shared_invite/zt-21upjsftv-gX~gNjTCU~2HfbeM_ZwTEQ#/shared-invite/email",
                    },
                    {
                        label: "Github",
                        href: "https://github.com/promptengineers-ai",
                    },
                    {
                        label: "Documentation",
                        href: "https://prompt-engineers.gitbook.io/documentation",
                    },
                ],
                starters: [
                    {
                        label: "Business Assessment",
                        template: "How does your agency assess my business's current state to identify opportunities for AI, software development, and marketing enhancements?",
                        variables: []
                    },
                    {
                        label: "AI Implementation Strategy",
                        template: "What strategy do you recommend for implementing AI solutions in businesses like mine?",
                        variables: []
                    },
                    {
                        label: "Software Development Solutions",
                        template: "What technologies and software development solutions do you offer to help businesses like mine?",
                        variables: []
                    },
                    {
                        label: "Marketing and AI",
                        template: "How can AI enhance our current marketing efforts and what are the first steps?",
                        variables: []
                    },
                    {
                        label: "ROI on AI Investments",
                        template: "How do you measure the ROI of AI and software development investments?",
                        variables: []
                    },
                    {
                        label: "Digital Transformation Roadmap",
                        template: "What does a digital transformation roadmap look like for a business looking to integrate AI and improve software and marketing?",
                        variables: []
                    },
                    {
                        label: "Training and Support",
                        template: "What kind of training and support do you provide to ensure our team can effectively use the new AI and software solutions?",
                        variables: []
                    },
                    {
                        label: "Data Security in AI Projects",
                        template: "How do you ensure data security and compliance in AI and software development projects?",
                        variables: []
                    },
                    {
                        label: "Competitive Advantage through AI",
                        template: "How can AI and advanced software development provide a competitive advantage to my business?",
                        variables: []
                    },
                    {
                        label: "Future-proofing with AI",
                        template: "How can your services help future-proof my business with AI and software innovations?",
                        variables: []
                    }
                ]
            }
        },
    });`;


export default function Loading() {
    return (
        <div className="flex text-white justify-center items-center min-h-screen">
            <h2>🌀 Loading...</h2>
        </div>
    );
}